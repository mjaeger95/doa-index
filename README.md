# doa-character-index

This script collects statistics for the archive page of dumbingofage.com

For each character listed in list.txt _and pair of characters_, counts results as listed on http://www.dumbingofage.com/tag/<name>+<name>/ in a Pandas Series and a DataFrame. For now, these are merely printed to the console.

I don't plan to continue working on this but encourage further development, especially visualization. 
